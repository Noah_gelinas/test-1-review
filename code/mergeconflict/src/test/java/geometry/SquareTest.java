package geometry;

import org.junit.*;
import static org.junit.Assert.assertEquals;

public class SquareTest {
    @Test 
    public void shouldReturnLength(){
        Square s=new Square(5);
        assertEquals(5,s.getLength());
    }

    @Test 
    public void shouldReturnArea() {
        Square s=new Square(5);
        assertEquals(25,s.getArea());
    }

    @Test
    public void shouldReturnString() {
        Square s=new Square(5);
        String expected="Area: "+s.getArea();
        assertEquals(expected,s.toString());
    }
}
