package geometry;

import org.junit.*;

import static org.junit.Assert.assertEquals;

public class TriangleTest {
    private Triangle triangleTest = new Triangle(4,5);
    @Test
    public void shouldReturnLength() {
        assertEquals(4, triangleTest.getLength());
    }

    @Test
    public void shouldReturnWidth() {
        assertEquals(5, triangleTest.getWidth());
    }

    @Test
    public void shouldReturn10() {
        assertEquals(10, triangleTest.getArea());
    }

    @Test
    public void shouldReturnStringWanted() {
        String expected = ("Width: " + triangleTest.getWidth() + "\nLength: " + triangleTest.getLength() + "\nArea: " + triangleTest.getArea());
        assertEquals(expected, triangleTest.toString());
    }
}
