package geometry;

public class Square{private int length;
    public Square(int length) {
        this.length=length;
    }

    public int getLength() {
        return this.length;
    }

    public int getArea() {
        return this.length*this.length;
    }

    public String toString() {
        return ("Area: "+getArea());
    }
}
