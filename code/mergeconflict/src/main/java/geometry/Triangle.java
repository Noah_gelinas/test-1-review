package geometry;

public class Triangle {
    private int length;
    private int width;
    
    public Triangle(int length, int width) {
        this.width = width;
        this.length = length;
    }

    public int getLength() {
        return this.length;
    } 

    public int getWidth() {
        return this.width;
    }

    public int getArea() {
        return (this.width*this.length)/2;
    }

    public String toString() {
        return ("Width: " + this.width + "\nLength: " + this.length + "\nArea: " + getArea());
    }
}